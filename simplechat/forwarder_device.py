__author__ = 'julian'

import datetime
import zmq
import chatmessage_pb2
import redis

def main():
    try:
        context = zmq.Context(1)
        # Socket facing clients
        frontend = context.socket(zmq.SUB)
        frontend.bind("tcp://*:5559")
        frontend.setsockopt(zmq.SUBSCRIBE, "")

        # Socket facing services
        backend = context.socket(zmq.PUB)
        backend.bind("tcp://*:5560")

        zeromq_relay(frontend, backend)
    except Exception, e:
        print e
        print "Bringing down zmq device"
    finally:
        pass
        frontend.close()
        backend.close()
        context.term()


def zeromq_relay(source, destination):
    """Copy and process data from socket source to socket destination """

    # Initial authorized ips, other can be added by issuing /auth ip.address.here
    authorized_ips = {"A1", "B1"}

    # Logging all messages to redis
    redis_client = redis.StrictRedis(host="localhost", port=6379, db=0)

    chatmessage = chatmessage_pb2.ChatMessage()

    while True:
        relay_message = source.recv()
        more = source.getsockopt(zmq.RCVMORE)

        topic, publisher_id, messagedata = relay_message.split(" ", 2)
        chatmessage.ParseFromString(messagedata)

        # Handles /auth ip.address.here
        if chatmessage.message.startswith('/auth '):
            _, new_ip = chatmessage.message.split(' ', 1)

            if not publisher_id == new_ip:
                authorized_ips.add(new_ip)
                print "%s added to authorized ips by %s" % (new_ip, publisher_id)
            else:
                print "%s trying to self-add" % publisher_id

        if publisher_id in authorized_ips:
            # The log key will be CHANNEL,NICKNAME,ISO-8601 DATETIME
            # The log value is going to be the serialized chatmessage object
            log_key = "%s,%s,%s" % (
                chatmessage.channel, chatmessage.nickname,
                (datetime.datetime.utcfromtimestamp(float(chatmessage.utcTimestamp))).isoformat())
            if not redis_client.set(log_key, messagedata):
                print "Could not log message to redis."

            # Route messages
            if more:
                destination.send(relay_message, zmq.SNDMORE)
            else:
                destination.send(relay_message)

        else:
            print "%s is not authorized to post to %s" % (publisher_id, topic)


if __name__ == "__main__":
    main()