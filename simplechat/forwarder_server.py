__author__ = 'julian'

import zmq
import sys
import datetime
import chatmessage_pb2


context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.connect("tcp://localhost:5559")
# This should be ip lookup instead of accepting parameter
publisher_id = sys.argv[1]
topic = "generalchat"
chatmessage = chatmessage_pb2.ChatMessage()

while True:
    # Solicit message
    messagedata = raw_input('message: ')

    chatmessage.utcTimestamp = "%f" % (datetime.datetime.utcnow() - datetime.datetime.fromtimestamp(0)).total_seconds()
    chatmessage.nickname = publisher_id
    chatmessage.channel = topic
    chatmessage.message = messagedata

    socket.send("%s %s %s" % (topic, publisher_id, chatmessage.SerializeToString()))
