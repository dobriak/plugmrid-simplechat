__author__ = 'julian'

import zmq
import chatmessage_pb2
import datetime


context = zmq.Context()
socket = context.socket(zmq.SUB)
print "Collecting updates from server..."
socket.connect("tcp://localhost:5560")
topicfilter = "generalchat"
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
chatmessage = chatmessage_pb2.ChatMessage()

while True:
    string = socket.recv()
    topic, publisher_id, messagedata = string.split(" ", 2)

    chatmessage.ParseFromString(messagedata)
    print (datetime.datetime.utcfromtimestamp(float(chatmessage.utcTimestamp))).isoformat()
    print "%s: %s" % (publisher_id, chatmessage.message)