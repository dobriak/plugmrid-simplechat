PLUMgrid simplechat
====================

Purpose
--------

Simple chat system that uses zeromq for transport, google protobuf for serialization and redis for logging.

Requirements:
-------------
 
 - Each client will be identified by it's IP address and the Port.
 - For a client to join the group it should only need the
 identification of any one client that is already in the group.
 - The tool requirement is that the transport uses zeromq messaging
 between the clients and protocol buffers as the message serialization
 transform.
 - Log all the chat messages in a redis database.
 - Take all the shortcuts necessary to finish the task in time. The
 architecture is not important for this assignment,  we are looking at
 the ability to use all the tools to make a functioning project. Dont
 worry about scalability of the system, put a maximum of 100 user's
 that can connect at the same time.
 Optionally , If time permits you can add a Display  message on
 everyone's receive window when a client joins or leaves.
 The goal is to create a working code with necessary build setup.
 Prioritize showing a working demo of the code over completing all the
 needed features.
 
 
Installation
-------------
 
Prerequisites:

  python 2.7.x, zeromq, pyzmq, protobuf (C and Python bindings), redis, redis-py. 
  Tested on linux, should work on Mac/Windows

Running the code:
----------------

Start your redis database

    /redis-server
    
Check out the repository somewhere on your machine:

    git clone https://bitbucket.org/dobriak/plugmrid-simplechat.git
    
Run the following commands each in its own terminal window:

    python forwarder_device.py
    python forwarder_subscriber.py
    python forwarder_server.py A1
    python forwarder_server.py C1
    
The chatters will be using the terminal windows running the forwarder_server.py program to enter text and the chat window that displays
the main chat room is the one running forwarder_subscriber.py.

I am passing A1 and C1 in lieu of an actual ip address (easier to test on the same machine).      
The middleware code (forwarder_device.py) comes with 2 pre authorized "ip" adresses: A1 and B1.
Thus the C1 one is not going to be authorized. In order to authorize it, from the "A1" window enter the following line:
    
    /auth C1

After entering a few messages in the A1 and C1 windows, check the log entries in redis:

    redis-cli
    KEYS *generalchat*
    
The log entry keys are in the following format: <chat room>,<chatter id>,<UTC ISO-8601 timestamp>. The log entries (values) are
 protobuf serialized messages with metadata (see chatmessage.proto for the fields). So in our case they should look
something like: Key: generalchat,A1,2015-3-8T11:12:13.1234567 Value: <serialized chatmessage protobuf object>.


